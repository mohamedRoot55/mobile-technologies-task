import 'package:floor/floor.dart';

@Entity(tableName: "Customer")
class Customer{
  @PrimaryKey(autoGenerate: true)
  int id  ;
  String firstName ;
  String lastName ;
  @ColumnInfo(name:"email" )
  String email ;
  @ColumnInfo(name:"passportId")
  String passportId ;
  DateTime dateOfBirth ;
  String imgPath ;
  String imei ;
  String deviceName  ;
  String latitude ;
  String longitude ;

  Customer({this.id, this.firstName, this.lastName, this.email, this.passportId,
    this.dateOfBirth, this.imgPath, this.imei ,this.deviceName , this.latitude , this.longitude});
}