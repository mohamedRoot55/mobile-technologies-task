import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'converter/DateTimeConverter.dart';
import 'customer_dao.dart';
import 'customer_entity.dart';

part 'app_dp.g.dart'; // the generated code will be there

@TypeConverters([
  DateTimeConverter
])
@Database(version: 1, entities: [Customer] )
abstract class AppDatabase extends FloorDatabase {
  CustomerDao get customerDao;
}