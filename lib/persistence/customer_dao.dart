import 'package:floor/floor.dart';

import 'customer_entity.dart';

@dao
abstract class CustomerDao {
  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insert(Customer customer) ;

  @Query("select * from Customer")
  Future<List<Customer>> getAllCustomers() ;

}