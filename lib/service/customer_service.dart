import 'package:mobile_tech_test/persistence/app_dp.dart';
import 'package:mobile_tech_test/persistence/customer_dao.dart';
import 'package:mobile_tech_test/persistence/customer_entity.dart';

class CustomerService {
  AppDatabase _appDatabase;

  CustomerDao _customerDao;

  Future initService() async {
    _appDatabase =
        await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    _customerDao = _appDatabase.customerDao;
  }

  Future insertCustomer(Customer customer) async {
    await _customerDao.insert(customer);
  }

  Future<List<Customer>> getAllCustomers() async {
    final customers = await _customerDao.getAllCustomers();
    return customers;
  }
}
