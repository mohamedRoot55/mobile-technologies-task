import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobile_tech_test/constants/imagePaths.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspaths;

class SelectImage extends StatefulWidget {
  final Function onImageSelected;
  final String imageUrl;

  SelectImage({@required this.onImageSelected, this.imageUrl = ""});

  @override
  _SelectImageState createState() => _SelectImageState();
}

class _SelectImageState extends State<SelectImage> {
  File _storedImage;
  BuildContext dialogContext;

  Future<void> _takePictureFromCamera() async {
    final imageFile = await ImagePicker.pickImage(
      source: ImageSource.camera,
      maxWidth: 600,
    );
    if (imageFile == null) {
      return;
    }
    final appDir = await syspaths.getApplicationDocumentsDirectory();
    final fileName = path.basename(imageFile.path);
    final savedImage = await imageFile.copy('${appDir.path}/$fileName');
    setState(() {
      _storedImage = savedImage;
    });
    widget.onImageSelected(_storedImage);
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return InkWell(
      onTap: _takePictureFromCamera,
      child: Container(
//      color: Colors.white,
        margin: EdgeInsets.only(top: 30),
        width: screenSize.width * .39,
        height: screenSize.width * .36,
        // color: orangeColor,
        child: Stack(
          children: <Widget>[
            _storedImage != null
                ? Container(
                    width: screenSize.width * .36,
                    height: screenSize.width * .36,
                    child: CircleAvatar(
                      backgroundImage: FileImage(
                        _storedImage,
                      ),
                    ),
                  )
                : widget.imageUrl != ""
                    ? Container(
                        width: screenSize.width * .36,
                        height: screenSize.width * .36,
                        child: CircleAvatar(
                          backgroundColor: Colors.yellow,
                          backgroundImage: NetworkImage(widget.imageUrl),
                        ),
                      )
                    : Container(
                        width: screenSize.width * .36,
                        height: screenSize.width * .36,
                        child: CircleAvatar(
                          //  backgroundColor: Colors.white,
                          backgroundImage: AssetImage(avatar),
                        ),
                      ),
            Center(
              child: Container(
                width: screenSize.width * .36,
                height: screenSize.width * .36,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Colors.black45),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Image.asset(
                        upload,
                        fit: BoxFit.cover,
                        width: 30,
                      ),
                    ),
                    Text(
                      "upload",
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
