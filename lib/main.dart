import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_tech_test/service/customer_service.dart';
import 'package:mobile_tech_test/ui/bloc/customer_bloc.dart';
import 'package:mobile_tech_test/ui/customer_registration.dart';

GetIt locator = GetIt.instance;

void setupSingleton() async {
  locator.registerLazySingleton<CustomerService>(() => CustomerService());
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupSingleton();
  CustomerService _customerService = GetIt.I.get<CustomerService>();
  await _customerService.initService();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<CustomerBloc>(
            create: (context) => CustomerBloc(),
          ),
        ],
        child: MaterialApp(
          title: 'Mobile-Technology-test',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: CustomerRegistration(),
        ));
  }
}
