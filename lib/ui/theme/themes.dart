import 'package:flutter/material.dart';

InputDecoration baseTextInputDecoration({BuildContext context , String labelText }){
  return InputDecoration(
    hintText:
    labelText,
    fillColor: Colors.white,
    enabledBorder: OutlineInputBorder(
      borderRadius:
      BorderRadius.circular(10),
      borderSide:
      BorderSide(color: Colors.blue),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius:
      BorderRadius.circular(10),
      borderSide:
      BorderSide(color: Colors.blue),
    ),
    border: OutlineInputBorder(
      borderRadius:
      BorderRadius.circular(10),
      borderSide:
      BorderSide(color: Colors.blue),
    ),
  ) ;
}