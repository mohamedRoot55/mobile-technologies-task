import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_tech_test/persistence/app_dp.dart';
import 'package:mobile_tech_test/persistence/customer_entity.dart';
import 'package:mobile_tech_test/ui/bloc/customer_bloc.dart';
import 'package:mobile_tech_test/ui/cutomer_info.dart';
import 'package:mobile_tech_test/ui/state.dart';

class CustomerDatabase extends StatefulWidget {
  @override
  _CustomerDatabaseState createState() => _CustomerDatabaseState();
}

class _CustomerDatabaseState extends State<CustomerDatabase> {
  List<Customer> customerHistory = [];
  CustomerBloc customerBloc ;

  bool isLoading = false;

  @override
  void initState() {

    super.initState();
    customerBloc = CustomerBloc() ;
    customerBloc.add(CustomerBlocEvent(type: CustomerBlocEventType.getAllCustomersFromDatabase ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("customers history"),
        centerTitle: true,
      ),
      body: BlocBuilder<CustomerBloc , GeneralBlocState<CustomerBlocState>>(
        bloc: customerBloc,
        builder: (ctx , state){
          if(state.hasData){
              customerHistory = state.data.customers ;
          }
          return Container(
            child: state.waiting
                ? Center(
              child: CircularProgressIndicator(),
            )
                : ListView.builder(
              itemBuilder: (ctx, index) {
                return ListTile(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (ctx) =>
                            CustomerInfo(customerHistory[index])));
                  },
                  title: Text(customerHistory[index].firstName),
                  subtitle: Text(customerHistory[index].email),
                );
              },
              itemCount:  customerHistory.isEmpty ? 0 :  customerHistory.length,
            ),
          ) ;
        },
      ),
    );
  }
}
