import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:get_mac/get_mac.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:mobile_tech_test/component/SelectImage.dart';
import 'package:mobile_tech_test/persistence/customer_entity.dart';
import 'package:mobile_tech_test/ui/bloc/customer_bloc.dart';
import 'package:mobile_tech_test/ui/theme/themes.dart';
import 'package:mobile_tech_test/utils/utils.dart';

import 'customer_data_history.dart';

class CustomerRegistration extends StatefulWidget {
  @override
  _CustomerRegisterationState createState() => _CustomerRegisterationState();
}

class _CustomerRegisterationState extends State<CustomerRegistration>
    with AfterLayoutMixin {
  File _pickedImage;
  LocationData _locationData;
  String deviceName;

  TextEditingController firstNameController,
      lastNameController,
      emailController,
      passportIdController,
      imeiController;

  DateTime dateOfBirth = DateTime.now();

  final formKey = GlobalKey<FormState>();

  final scaffoldKey = GlobalKey<ScaffoldState>();

  CustomerBloc _customerBloc;

  @override
  void initState() {
    super.initState();
    firstNameController = TextEditingController();
    lastNameController = TextEditingController();
    emailController = TextEditingController();
    passportIdController = TextEditingController();
    imeiController = TextEditingController();

    _customerBloc = CustomerBloc();
  }

  Future initialPlatformState() async {
    String imei = "";
    try {
      if (Platform.isAndroid) {
        imei = await ImeiPlugin.getImei(
            shouldShowRequestPermissionRationale: false);
      } else {
        imei = await GetMac.macAddress;
      }
      setState(() {
        imeiController.text = imei;
      });
    } catch (error) {
      print("initialPlatformState ERROR : $error");
    }
  }

  Future save() async {
    final isValidate = formKey.currentState.validate();
    if (_pickedImage == null) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Please Take Profile Image")));
      return;
    }
    if (isValidate) {
      Customer customer = Customer(
          firstName: firstNameController.text,
          lastName: lastNameController.text,
          email: emailController.text,
          passportId: passportIdController.text,
          dateOfBirth: dateOfBirth,
          imei: imeiController.text,
          imgPath: _pickedImage.path,
          latitude: _locationData.latitude.toString(),
          longitude: _locationData.longitude.toString(),
          deviceName: deviceName);
      _customerBloc.add(CustomerBlocEvent(type: CustomerBlocEventType.insertNewCustomer, customer: customer));
      Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => CustomerDatabase()));
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    initializeDateFormatting('en');
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("Register"),
        centerTitle: true,
      ),
      body: Form(
        key: formKey,
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(7),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SelectImage(
                  onImageSelected: (_selectedImg) {
                    setState(() {
                      _pickedImage = _selectedImg;
                    });
                  },
                ),
                SizedBox(
                  height: size.height * .04,
                ),
                TextFormField(
                  controller: firstNameController,
                  decoration: baseTextInputDecoration(context: context ,labelText : "First Name")   ,
                  textInputAction: TextInputAction.next,
                  onSaved: (value) {
                    firstNameController.text = value;
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return "";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: size.height * .01,
                ),
                TextFormField(
                  controller: lastNameController,
                  decoration: baseTextInputDecoration(context: context ,labelText : "Last Name")   ,
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (_) {},
                  onSaved: (value) {
                    lastNameController.text = value;
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return "";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: size.height * .01,
                ),
                TextFormField(
                  controller: emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: baseTextInputDecoration(context: context ,labelText : "Email")   ,
                   textInputAction: TextInputAction.done,
                  onSaved: (value) {
                    emailController.text = value;
                  },
                ),
                SizedBox(
                  height: size.height * .01,
                ),
                TextFormField(
                  controller: imeiController,
                  decoration: baseTextInputDecoration(context: context ,labelText : "IMEI")   ,
                   onSaved: (value) {
                    imeiController.text = value;
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      // return S.of(context).meetingDateValidation;
                      return "";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: size.height * .01,
                ),
                DateTimeField(
                  decoration: baseTextInputDecoration(context: context ,labelText : "Date Of Birth")   ,
                   validator: (value) {
                    if (value == null) {
                      // return S.of(context).meetingDateValidation;
                      return "";
                    } else {
                      return null;
                    }
                  },
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                  format: DateFormat("dd/MM/yyyy"),
                  onShowPicker: (context, currentValue) async {
                    final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        locale: Locale("en"),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                    if (date != null) {
                      setState(() {
                        dateOfBirth = date;
                      });
                    }
                    return date;
                  },
                ),
                SizedBox(
                  height: size.height * .01,
                ),
                if (DateTime.now().year - dateOfBirth.year > 18)
                  TextFormField(
                    controller: passportIdController,
                    decoration: baseTextInputDecoration(context: context ,labelText : "Passport Id")   ,
                    textInputAction: TextInputAction.done,
                    onSaved: (value) {
                      passportIdController.text = value;
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        // return S.of(context).meetingDateValidation;
                        return "";
                      } else {
                        return null;
                      }
                    },
                  ),
                ElevatedButton(
                  onPressed: () async {
                    await save();
                  },
                  child: Text("save and navigate to customers history"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    await initialPlatformState();
    final currentLocationInfo = await Utils.requestLocationData();
    if (currentLocationInfo != null) {
      _locationData = currentLocationInfo;
    }
    deviceName = await Utils.requestDeviceInfo();
  }
}
