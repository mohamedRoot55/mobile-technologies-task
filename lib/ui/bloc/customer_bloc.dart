import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_tech_test/persistence/customer_entity.dart';
import 'package:mobile_tech_test/service/customer_service.dart';
import 'package:mobile_tech_test/ui/state.dart';

enum CustomerBlocEventType { insertNewCustomer, getAllCustomersFromDatabase }

class CustomerBlocEvent {
  final CustomerBlocEventType type;

  final Customer customer;

  CustomerBlocEvent({this.type, this.customer});
}

class CustomerBlocState {
  final List<Customer> customers;

  CustomerBlocState({this.customers});
}

class CustomerBloc
    extends Bloc<CustomerBlocEvent, GeneralBlocState<CustomerBlocState>> {
  CustomerService _customerService = GetIt.I.get<CustomerService>();

  GeneralBlocState<CustomerBlocState> get initialState => GeneralBlocState();

  List<Customer> _customers = [];

  @override
  Stream<GeneralBlocState<CustomerBlocState>> mapEventToState(
      CustomerBlocEvent event) async* {
    try {
      if (event.type == CustomerBlocEventType.insertNewCustomer) {
        yield GeneralBlocState(waiting: true);
        await addNewCustomer(event.customer);
      } else if (event.type ==
          CustomerBlocEventType.getAllCustomersFromDatabase) {
        yield GeneralBlocState(waiting: true);
        await getAllCustomers();
        yield GeneralBlocState(data: CustomerBlocState(customers: _customers) , hasData: true);
      }
    } catch (error) {
      yield GeneralBlocState(error: "UNKNOWN ERROR", hasError: true);
    }
  }

  Future addNewCustomer(Customer customer) async {
    await _customerService.insertCustomer(customer);
  }

  Future getAllCustomers() async {
    final customers = await _customerService.getAllCustomers();
    print("customers $customers") ;
    _customers = customers;
  }
}
