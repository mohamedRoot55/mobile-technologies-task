import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mobile_tech_test/persistence/customer_entity.dart';

class CustomerInfo extends StatelessWidget {
  final Customer customer;

  CustomerInfo(this.customer);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Customer Info"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            height: size.height * .4,
            child: Image.file(
              File(customer.imgPath),
              fit: BoxFit.cover,
            ),
          ),
          Text("FirstName ${customer.firstName}"),
          Text("LastName ${customer.lastName}"),
          Text("Email ${customer.email}"),
          Text("IMEI ${customer.imei}"),
          Text("passportId ${customer.passportId}"),
          Text("longitude ${customer.longitude}"),
          Text("latitude ${customer.latitude}"),
          Text("dateOfBirth ${customer.dateOfBirth}"),
          Text("deviceName ${customer.deviceName}"),
        ],
      ),
    );
  }
}
